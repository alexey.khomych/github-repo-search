//
//  RepoService.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation
import Alamofire

struct APIError: Error {
    let errorDescription: String
}

protocol RepoService {
    func find(with models: [RepoInfoRequestModel], completion: @escaping (Swift.Result<[RepoModel], APIError>) -> ())
    func cancelSearching()
}

class RepoServiceImpl {
    
    // MARK: Private Variable
    
    private var isSearching = false
    private var operationQueue: OperationQueue {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = Constants.maxConcurrentOperationCount
        return operationQueue
    }
    private var pendingRequets = [DataRequest]()
    
    private let accessQueue = DispatchQueue(label: Constants.accessQueueLabel, attributes: .concurrent)

    private var items: [RepoModel] = []
}

// MARK: - Private

private extension RepoServiceImpl {
    
    enum Constants {
        static let accessQueueLabel = "com.repoService.dispatchBarrier"
        static let maxConcurrentOperationCount = 2
    }
    
    func updateItems(_ items: [RepoModel]) {
        accessQueue.async(flags: .barrier) {
            self.items += items
        }
    }
}

// MARK: RepoService

extension RepoServiceImpl: RepoService {
    
    func find(with models: [RepoInfoRequestModel], completion: @escaping (Swift.Result<[RepoModel], APIError>) -> ()) {
        let dispatchGroup = DispatchGroup()
        var storedError: APIError?
        
        isSearching = true
        
        operationQueue.addOperations(models.compactMap { requestModel -> BlockOperation in
            dispatchGroup.enter()
            return BlockOperation {
                let router = RepoInfoRouter.findRepo(requestModel)
                let request = Alamofire.request(router).responseJSON { (dataResponse) in
                    defer { dispatchGroup.leave() }
                    switch dataResponse.result {
                    case .success:
                        if let data = dataResponse.data {
                            do {
                                let response = try JSONDecoder().decode(ResponseModel.self, from: data)
                                self.updateItems(response.items ?? [])
                            } catch {
                                storedError = APIError(errorDescription: "Corruped response")
                            }
                        }
                    case let .failure(error):
                        storedError = APIError(errorDescription: error.localizedDescription)
                    }
                }

                self.pendingRequets.append(request)
            }
        }, waitUntilFinished: false)
        
        dispatchGroup.notify(queue: .main) {
            if let storedError = storedError {
                completion(.failure(storedError))
            } else {
                completion(.success(self.items))
                self.items.removeAll()
            }
            
            self.isSearching = false
        }
    }
    
    func cancelSearching() {
        if !isSearching {
            return
        }
        operationQueue.cancelAllOperations()
        pendingRequets.forEach { $0.cancel() }
        isSearching = true
    }
}
