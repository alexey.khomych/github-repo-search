//
//  Router.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Alamofire

enum RepoInfoRouter {
    case findRepo(RepoInfoRequestModel)
}

extension RepoInfoRouter: BaseRouterProtocol {
    
    var httpMethod: HTTPMethod {
        switch self {
        case .findRepo:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .findRepo:
            return "search/repositories"
        }
    }
    
    var queryParameters: [String : String]? {
        switch self {
        case let .findRepo(model):
            return ["q": model.keywords,
                    "sort": model.sort,
                    "order": model.order,
                    "page": model.page,
                    "per_page": model.perPage]
        }
    }
}
