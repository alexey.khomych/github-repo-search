//
//  RepoInfoRequestModel.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

struct RepoInfoRequestModel: Encodable {
    let keywords: String
    let sort: String
    let order: String
    let page: String
    let perPage: String
}

enum SortType: String {
    case stars = "stars"
    case forks = "forks"
    case updated = "updated"
}

extension RepoInfoRequestModel {
    
    init(keywords: String, sort: SortType , order: String, page: Int, perPage: Int) {
        self.keywords = keywords
        self.sort = sort.rawValue
        self.order = order
        self.page = "\(page)"
        self.perPage = "\(perPage)"
    }
}
