//
//  BaseRouterProtocol.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 27.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Alamofire

protocol BaseRouterProtocol: URLRequestConvertible {
    var additionalHeaders: [String: String]? { get }
    var httpMethod: Alamofire.HTTPMethod { get }
    var path: String { get }
    var queryParameters: [String: String]? { get }
    var baseURL: String { get }
}

extension BaseRouterProtocol {
    
    var baseURL: String {
        return "https://api.github.com/"
    }

    func asURLRequest() throws -> URLRequest {
        guard let url = URL(string: baseURL.appending(path)) else {
            fatalError("No path components in BaseRouterProtocol")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.allHTTPHeaderFields = ["Content-Type": "application/json"]
        urlRequest.httpMethod = httpMethod.rawValue
        additionalHeaders?.forEach { urlRequest.setValue($0.value, forHTTPHeaderField: $0.key) }

        if let parameters = queryParameters {
            urlRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}

extension BaseRouterProtocol {
    var additionalHeaders: [String: String]? { return nil }
    var queryParameters: [String: String]? { return nil }
}
