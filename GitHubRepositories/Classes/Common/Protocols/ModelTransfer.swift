//
//  ModelTransfer.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

protocol ModelTransfer {
    /// Type of model that is being transferred
    associatedtype ModelType

    /// Updates view with `model`.
    func update(with model: ModelType)

    /// Appends items to excisting datasource, simplify logic (Optional)
    func appendItems(with model: ModelType)
}

extension ModelTransfer {
    
    func appendItems(with model: ModelType) {

    }
}
