//
//  GlobalConstants.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 26.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

struct GlobalConstants {
    static let okTitle = "Ok"
    static let cancelTitle = "Cancel"
}
