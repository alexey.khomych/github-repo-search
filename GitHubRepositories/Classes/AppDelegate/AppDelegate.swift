//
//  AppDelegate.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let storyBoard = UIStoryboard(name: "RepoSearchViewController", bundle: nil)
        let id = String(describing: "RepoSearchViewController")
        let controller = storyBoard.instantiateViewController(withIdentifier: id)
        window?.rootViewController = controller
        window?.makeKeyAndVisible()
        
        return true
    }
}
