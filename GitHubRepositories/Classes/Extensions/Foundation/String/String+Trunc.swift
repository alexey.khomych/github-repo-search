//
//  String+Trunc.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 26.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

extension String {
    func trunc(length: Int, trailing: String = "…") -> String {
        return (count > length) ? prefix(length) + trailing : self
    }
}
