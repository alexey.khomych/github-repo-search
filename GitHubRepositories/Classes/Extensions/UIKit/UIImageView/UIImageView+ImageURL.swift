//
//  UIImageView+ImageURL.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Kingfisher

extension UIImageView {

    func setImage(with url: URL) {
        kf.setImage(with: url)
    }
}
