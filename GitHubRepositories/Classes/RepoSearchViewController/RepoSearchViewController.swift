//
//  RepoSearchViewController.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class RepoSearchViewController: UIViewController {
    
    // MARK: IBOutlets
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchTextField: UITextField!
    @IBOutlet private weak var searchButton: UIButton!
    
    // MARK: Private Variables
    
    private var dataSource = ArrayTableViewDataSource<RepoModel, RepoSearchTableViewCell>()
    private let repoService = RepoServiceImpl()
    
    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
}

// MARK: Private

private extension RepoSearchViewController {
    
    enum Constants {
        static let minValuesCount = 0
        static let reposPerPage = 15
        static let orderType = "descending"
    }
    
    @IBAction func onSearchTap() {
        guard let keywords = searchTextField.text else { return }
        repoService.cancelSearching()
        
        let requestModels = configureRequestModels(with: keywords, modelsCount: 2)
        
        repoService.find(with: requestModels) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case let .success(repos):
                self.updateDataSource(repos)
                break
            case let .failure(error):
                self.presentAlert(.error(message: error.errorDescription))
                break
            }
        }
    }
    
    func setupUI() {
        tableView.dataSource = dataSource
        searchTextField.addTarget(self, action: #selector(searchTextFieldValueDidChange), for: .editingChanged)
    }
    
    @objc func searchTextFieldValueDidChange(textField: UITextField) {
        searchButton.isEnabled = textField.text?.count ?? Constants.minValuesCount > Constants.minValuesCount
    }
    
    func updateDataSource(_ repos: [RepoModel]) {
        dataSource.updateWith(items: repos)
        tableView.reloadData()
    }
    
    func configureRequestModels(with keywords: String, modelsCount: Int) -> [RepoInfoRequestModel] {
        var requestModels = [RepoInfoRequestModel]()
        
        for page in 1...modelsCount {
            let model = RepoInfoRequestModel(keywords: keywords,
            sort: .stars,
            order: Constants.orderType,
            page: page,
            perPage: Constants.reposPerPage)
            requestModels.append(model)
        }
        
        return requestModels
    }
}

// MARK: UITableViewDelegate

extension RepoSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
