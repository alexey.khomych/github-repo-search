//
//  RepoSearchTableViewCell.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import UIKit

class RepoSearchTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var logoImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var starsCountLabel: UILabel!
    @IBOutlet private weak var languageLabel: UILabel!
    
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }
    
    // MARK: - View lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        logoImageView.layer.cornerRadius = 30
        logoImageView.clipsToBounds = true
    }
}

// MARK: - ModelTransfer

extension RepoSearchTableViewCell: ModelTransfer {
    
    typealias ModelType = RepoModel
    
    func update(with model: RepoModel) {
        if let url = URL(string: model.owner.avatarURL) {
            logoImageView.setImage(with: url)
        }
        
        titleLabel.text = model.fullName.trunc(length: 30)
        starsCountLabel.text = "Stars: \(model.stargazersCount)"
        languageLabel.text = "Language: \(model.language ?? "")"
    }
}
