//
//  LicenseModel.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 1/25/20.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

struct License: Codable {
    let key, name, spdxID: String
    let url: String?
    let nodeID: String

    enum CodingKeys: String, CodingKey {
        case key, name
        case spdxID = "spdx_id"
        case url
        case nodeID = "node_id"
    }
}
