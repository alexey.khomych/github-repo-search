//
//  SomeModel.swift
//  GitHubRepositories
//
//  Created by Alexey Khomych on 26.01.2020.
//  Copyright © 2020 Alexey Khomych. All rights reserved.
//

import Foundation

// MARK: - SomeModel
struct ResponseModel: Codable {
    let totalCount: Int?
    let incompleteResults: Bool?
    let items: [RepoModel]?

    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case items
    }
}
