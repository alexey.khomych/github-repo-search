# GitHub repo search

Test task.

Description

The application allows search of GitHub repositories in parallel (parallel search).

* The screen must not be blocked during the search (you can scroll searching results).
* Should display first 30 repositiries based on query (first 15 from first thread and remaining from second thread).
* Repositories should be sorted by stars.
* Should be able to cancel search.
* Limit strings to 30 characters.
